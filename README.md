# 飞行控制系统 课程设计

- `documentclass`中的`{ctexart}`能保证汉字的正常显示。
- [mcode](https://github.com/nasa/nasa-latex-docs/blob/master/support/packages/mcode/mcode.sty)模块用于向LaTeX文档中插入MATLAB代码块。需要将`mcode.sty`文件置于文档根目录，再通过`\usepackage{mcode}`调用。
- `\CTEXsetup[format={\Large\bfseries}]{section}`可将section的标题由默认的居中改为左对齐。
- `\setcounter{section}{-1}`将section的序号设置为从**0**开始而不是**1**。
- 插入来自MATLAB的图片：  
  1. MATLAB将图片保存为`.eps`文件
  2. 在tex文档中加入： 
      ```
      \usepackage{graphicx}
      \usepackage{epstopdf}
      \usepackage{float}
      ```
  3. 在需要插入图片的地方加入:
      ```
      \begin{figure}[H]
      %H表示将图形放置在文中给出该图形环境的地方
      \centering
      \includegraphics[height=2cm,width=3cm]{path/to/fig.eps}
      %中括号及其中的内容可省略
      \caption{这是图片的描述}
      \label{1}
      \end{figure}
      ```
