\contentsline {section}{\numberline {0}实验目标及准备内容}{1}% 
\contentsline {paragraph}{实验目标}{1}% 
\contentsline {paragraph}{准备内容}{1}% 
\contentsline {section}{\numberline {1}飞机纵向飞行控制系统的设计和仿真}{2}% 
\contentsline {subsection}{\numberline {1.1}飞机纵向自然特性的分析}{2}% 
\contentsline {paragraph}{问题}{2}% 
\contentsline {subsection}{\numberline {1.2}飞机纵向自然特性的仿真}{2}% 
\contentsline {paragraph}{问题}{2}% 
\contentsline {subsubsection}{\numberline {1.2.1}升降舵}{2}% 
\contentsline {subsubsection}{\numberline {1.2.2}油门}{3}% 
\contentsline {subsection}{\numberline {1.3}飞机俯仰角控制系统的设计}{5}% 
\contentsline {paragraph}{问题}{5}% 
\contentsline {subsection}{\numberline {1.4}飞机速度控制系统的设计}{9}% 
\contentsline {paragraph}{问题}{9}% 
\contentsline {subsection}{\numberline {1.5}飞机纵向运动的仿真与分析}{10}% 
\contentsline {paragraph}{问题}{10}% 
\contentsline {section}{\numberline {2}飞机侧向飞行控制系统的设计和仿真}{14}% 
\contentsline {subsection}{\numberline {2.1}飞机侧向自然特性的分析与仿真}{14}% 
\contentsline {paragraph}{问题}{14}% 
\contentsline {paragraph}{}{14}% 
\contentsline {paragraph}{问题}{14}% 
\contentsline {subsubsection}{\numberline {2.1.1}副翼}{14}% 
\contentsline {subsubsection}{\numberline {2.1.2}方向舵}{15}% 
\contentsline {subsection}{\numberline {2.2}飞机滚转角控制系统的设计}{17}% 
\contentsline {paragraph}{问题}{17}% 
\contentsline {subsection}{\numberline {2.3}飞机航向控制系统的设计}{20}% 
\contentsline {paragraph}{问题}{20}% 
\contentsline {subsection}{\numberline {2.4}飞机侧向航向协调控制仿真与分析}{22}% 
\contentsline {paragraph}{问题}{22}% 
\contentsline {section}{\numberline {3}对提高飞行控制系统课程教学效果提出的建议与意见}{25}% 
\contentsline {subsection}{\numberline {3.1}建议}{25}% 
\contentsline {subsection}{\numberline {3.2}意见}{26}% 
\contentsline {paragraph}{}{26}% 
